<div class="container-fluid-full">
<div class="row-fluid">

	<!-- start: Main Menu -->
	<div id="sidebar-left" class="span2">
		<div class="nav-collapse sidebar-nav">
			<ul class="nav nav-tabs nav-stacked main-menu">
				<li><a href="<?php echo base_url();?>Users_Controller/login_user"><i class="icon-dashboard"></i><span class="hidden-tablet"> Dashboard</span></a></li>
				<li><a href="#"><i class="icon-user"></i><span class="hidden-tablet"> Users</span></a></li>

				<li><a href="#"><i class="icon-envelope"></i><span class="hidden-tablet"> Create Bid</span></a></li>

				<li>
					<a class="dropmenu" href="#">
					<div class="dropmenu">
						<i class="icon-reorder"></i><span class="hidden-tablet"> Request</span>

					</div>
					</a>
					<ul>
						<li><a href="#"><i class="icon-envelope"></i><span class="hidden-tablet"> Create Request</span></a></li>
						<li><a href="#"><i class="icon-tasks"></i><span class="hidden-tablet"> Approve Request</span></a></li>
						<li><a href="#"><i class="icon-eye-open"></i><span class="hidden-tablet"> Display Request</span></a></li>
					<!--	<li><a class="submenu" href="#"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 2</span></a></li>
						<li><a class="submenu" href="#"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 3</span></a></li>
					</ul>	-->
				</ul>
				</li>
				<li><a href="#"><i class="icon-edit" aria-hidden="true"></i><span class="hidden-tablet"> Create Auction</span></a></li>
				<li><a href="#"><i class="icon-facetime-video"></i><span class="hidden-tablet"> Goto Live Auction</span></a></li>
				<li><a href="#"><i class="icon-off"></i><span class="hidden-tablet"> Logout</span></a></li>
<!-- 				<li><a href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Forms</span></a></li>
				<li><a href="chart.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Charts</span></a></li>
				<li><a href="typography.html"><i class="icon-font"></i><span class="hidden-tablet"> Typography</span></a></li>
				<li><a href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
				<li><a href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
				<li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
				<li><a href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
				<li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
				<li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li> -->
		</div>
	</div>
