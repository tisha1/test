<?php
 if( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Dashboard extends CI_controller//controller class login
 {
	function __construct()
			{
				parent::__construct();
				$this->load->helper('url');
				$this->load->library('form_validation');
				$this->load->library('session');
        	$this->load->model('Dashboard_model');
		}

				//-------this is a function for login-----//
		 public function index()
		 {
$post=$this->input->post();
$this->form_validation->set_rules('sur_name','sur_name','trim|required');
$this->form_validation->set_rules('description','description','trim|required');


if($this->form_validation->run()==FALSE)
{
	$this->load->view('survey');

}
else {
			if(!empty($post))
	{
		$id= $this->Dashboard_model->Add_survey($post);
if($id)
{
	$sult['s_id']=$id;

					$this->session->set_userdata($sult);
$res['info']= $this->Dashboard_model->fetch();
$this->load->view('surveylist',$res);
}
	}

}
}



public function question()
{
	$id=$this->session->userdata('s_id');
// print_r($id);die;
$post=$this->input->post();
$this->form_validation->set_rules('ques','ques','trim|required');
$this->form_validation->set_rules('q1a','q1a','trim|required');
$this->form_validation->set_rules('q1b','q1b','trim|required');
$this->form_validation->set_rules('q1c','q1c','trim|required');
$this->form_validation->set_rules('q1d','q1d','trim|required');
$this->form_validation->set_rules('c1','c1','trim|required');
if($this->form_validation->run()==FALSE)
{
	$this->load->view('question');

}
else {
			if(!empty($post))
	{

$data=array('s_id' =>$id,'question' => $this->input->post('ques'),'choice1' =>$this->input->post('q1a'),'choice2' => $this->input->post('q1b'),'choice3' =>$this->input->post('q1c'),'choice4'=>$this->input->post('q1d'));
$data['answer']=$this->input->post('c1');
$res=$this->Dashboard_model->add_ques_choice($data);
}
}
 //class closed//
}


public function listing()
{
$data=$this->Dashboard_model->fetch();
}
}
?>
